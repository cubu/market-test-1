var Equipment = artifacts.require('Equipment');

contract('Equipment', function (accounts){
    var helpfulFunctions = require('./utils/EquipmentUtils')(Equipment, accounts);
    var hfn = Object.keys(helpfulFunctions);
    for(var i= 0; i<hfn.length; i++){
        global[hfn[i]] = helpfulFunctions[hfn[i]];
    }

    checkTotalSupply(0);


    
    for(x=0; x < 100; x++){
        checkEquipmentCreation('Equipment-' + x);
    }

    checkTotalSupply(100);
})