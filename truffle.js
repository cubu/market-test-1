require('dotenv').config();

const HDWalletProvider = require('truffle-hdwallet-provider');

const providerWithMnemonic = (mnemonic, rpcEndpoint) =>
  new HDWalletProvider(mnemonic, rpcEndpoint);

var infura_apikey = "d702dfa9f32145ab90aa37f949c1dc5e";
var mnemonic = "candy maple cake sugar pudding cream honey rich smooth crumble sweet treat";
var address = "0x76A9B373c04d9505Ff89bB354B7C67454764A68F"

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*',
    },
    testrpc: {
      host: 'localhost',
      port: 8545,
      network_id: '*',
    },
    ganache: {
      host: 'localhost',
      port: 7545,
      network_id: '*',
    },
    ropsten: {
      provider: new HDWalletProvider(mnemonic, 
                "https://ropsten.infura.io/" + infura_apikey),
      network_id: 3,
      gas: 400000
  }
  },
};
