/* Main Object to manage Contract interactions */
var App = {

  contracts: {},
  EquipmentContractAddress: '0x06efb10298e01e6412b22e11d7becee2a41c909b', 

  init() {
    return App.initWeb3();
  },
  
  initWeb3() {
    return App.initContract();
  },

  initContract(){
    $.getJSON('Equipment.json', (data) => {
        const EquipmentArtifact = data;
        App.contracts.Equipment = TruffleContract(EquipmentArtifact);
        App.contracts.Equipment.setProvider(web3.currentProvider);
        return App.loadEquipments();
    });
    return App.bindEvents();
  },

  loadEquipments(){
    web3.eth.getAccounts(function(err, accounts){
      if(err != null){
        console.error("An error occurred" + err);
      }else if(accounts.length == 0){
        console.log("User is not logged into MetaMask");
      }else{
        $('#card-row').children().remove();
      }
    });

    var address = web3.eth.defaultAccount;
    let contractInstance = App.contracts.Equipment.at(App.EquipmentContractAddress);
    return totalSupply  = contractInstance.totalSupply().then((supply) => {
      for (var i=0; i<supply; i++){
        App.getEquipmentDetails(i, address);
      }
    }).catch((err)=>{
      console.log(err.message);
    })
  },

  getEquipmentDetails(equipmentId, localAddress){
    let contractInstance = App.contracts.Equipment.at(App.EquipmentContractAddress);
    return contractInstance.getToken(equipmentId).then((equipment) => {
      var equipmentJson = {
        'equipmentId': equipmentId,
        'equipmentName': equipment[0],
        'equipmentDna': equipment[1],
        'equipmentPrice': web3.fromWei(equipment[2]).toNumber(),
        'equipmentNextPrice': web3.fromWei(equipment[3]).toNumber(),
        'ownerAddress': equipment[4]
      };
      if(localAddress !== equipmentJson.ownerAddress){
        loadEquipment(
          equipmentJson.equipmentId,
          equipmentJson.equipmentName,
          equipmentJson.equipmentDna,
          equipmentJson.equipmentPrice,
          equipmentJson.equipmentNextPrice,
          equipmentJson.ownerAddress,
          false);
      }else{
        loadEquipment(
          equipmentJson.equipmentId,
          equipmentJson.equipmentName,
          equipmentJson.equipmentDna,
          equipmentJson.equipmentPrice,
          equipmentJson.equipmentNextPrice,
          equipmentJson.ownerAddress,
          true);
      }
    }).catch((err)=>{
      console.log(err.message);
    })
  },

  handlePurchase(event){
    event.preventDefault();

    var equipmentId = parseInt($(event.target.elements).closest('.btn-buy').data('id'));

    web3.eth.getAccounts((error, accounts)=>{
      if(error){
        console.log(error);
      }
      var account = accounts[0];

      let contractInstance = App.contracts.Equipment.at(App.EquipmentContractAddress);
      contractInstance.priceOf(equipmentId).then((price) => {
        return contractInstance.purchase(equipmentId,{
          from:account,
          value: price,
        }).then(result => App.loadEquipments()).catch((err) => {
          console.log(err.message);
        });
      });
    });
  },

  bindEvents() {
    $(document).on('submit', 'form.equipment-purchase', App.handlePurchase);
  },







};

/* Generates a equipment image based on equipment DNA */
function generateEquipmentImage(equipmentId, size, canvas){
  
}

/*load based on input data*/
function loadEquipment(equipmentId, equipmentName, equipmentDna, equipmentPrice, equipmentNextPrice, ownerAddress, locallyOwned){
  var cardRow = $('#card-row');
  var cardTemplate = $('#card-template');

  if(locallyOwned){
    cardTemplate.find('btn-buy').attr('disabled', true);
  }else{
    cardTemplate.find('btn-buy').removeAttr('disabled');
  }

  cardTemplate.find('.equipment-name').text(equipmentName);
  cardTemplate.find('.equipment-canvas').attr('id', "equipment-canvas-" + equipmentId);
  cardTemplate.find('.equipment-dna').text(equipmentDna);
  cardTemplate.find('.equipment-owner').text(ownerAddress);
  cardTemplate.find('.equipment-owner').attr("href", "https://etherscan.io/address/" + ownerAddress);
  cardTemplate.find('.btn-buy').attr("data-id", equipmentId);
  cardTemplate.find(".equipment-price").text(parseFloat(equipmentPrice).toFixed(4));
  cardTemplate.find(".equipment-next-price").text(parseFloat(equipmentNextPrice).toFixed(4));

  cardRow.append(cardTemplate.html());
  generateEquipmentImage(equipmentDna, 3, "equipment-canvas-" + equipmentId);

}
window.addEventListener('load', async () => {
  // Modern dapp browsers...
  if (window.ethereum) {
      window.web3 = new Web3(ethereum);
      try {
          // Request account access if needed
          await ethereum.enable();
          // Acccounts now exposed
      } catch (error) {
          // User denied account access...
      }
  }
  // Legacy dapp browsers..
  else if (window.web3) {
      window.web3 = new Web3(web3.currentProvider);
      // Acccounts always exposed
      
  }
  // Non-dapp browsers...
  else {
      console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
  }
  App.init();
});
